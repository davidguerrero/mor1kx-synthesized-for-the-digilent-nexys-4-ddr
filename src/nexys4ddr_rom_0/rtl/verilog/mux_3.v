`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.10.2017 17:51:27
// Design Name: 
// Module Name: mux_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_3(
    input [WIDTH:0] a,
    input [WIDTH:0] b,
    input [WIDTH:0] c,
    input [1:0] sel,
    output [WIDTH:0] d
    );

parameter WIDTH = 0;

reg [WIDTH:0] value;
always @(*)
begin
    case (sel)
        2'b00: value = a;
        2'b01: value = b;
        2'b10: value = c;
        2'b11: value = a; 
    endcase
end

assign d = value;

endmodule